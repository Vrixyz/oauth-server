Thanks for your interest.

To contribute, you can check the issues labeled "help wanted", they are the most detailed and I'm ready to mentor. Drop a comment, we'll see from there.

That said, I don't have much time to spend on this project, so contributions will be accepted at a slow rate.

Be kind, be constructive, be modest, and be welcome :)