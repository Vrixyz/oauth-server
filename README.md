This is an generic oauth server.

**This project is NOT ready for production.**

Its goal is to provide a small independent codebase to manage token generation for another Resource Server.

# Features
token generation currently includes:
- Verification of an access token to a third party (Facebook)
- Retrieve a specific user id for the client to use in Resource Server.
- Signing the token with a secret stored in a .env file
- TODO: (soonish) expiry for tokens
- TODO: (soonish) refresh capability
- TODO: (later) handle different resource servers

# Getting started
- You need nightly rust, because rocket.rs is used.
- An uncommited .dotenv is necessary for the app to work. a [.dotenv.sample](/.dotenv.sample) is provided to get you started.
- `cargo run` should get you going.

# Technical details

## How it works
I use [planText](https://www.planttext.com/) to generate this ([from this file](/doc/diagram.plantuml)):

![Authentication sequence diagram](https://www.plantuml.com/plantuml/img/RLF1JiCm3BtdAwoUG1fmtm7O9WrD877O1o0lzLgBDgaITz2_nsdfsasxH6ho_FpUi_iQ1OEqTMMCi5G4sQgLafomHM7lCcCEldKvXWwoDwm9LasJ0KP87vkA5MgiTn9y1TaMBHsy_-u1s_NqC5GgC-mfd2WC3AkzIPrafFxm1TiBnCVU63tWuJcXOGab1GAEK7SGoGQI1RHHRmJ4G-J2WLxcYHMh-8JMKepYlyb5CmbETDkriXKaSFpNF2yq-ffQJXNBcpYu8Xqq9jOY-PaZVrZDdpdm8cHeWlT7awf7PQWs5H8lIQmFWQp0ooYmxzTH76jJLakmwA3pRI-axda6FKs57GNW78u-fDp284jA5CX6SL-SPzhX8lRFzmNiaXGKobFQMgydLdgJTq2_7FLXCKnWukJyyQnidcM2AKy_zCtCtrrQ66J7hfWhLaLf6s9xkByElFUxWn9FOrxNf8lhE9h09xK1x3I5UXZ0P7SkSmUwY7nabNfxdhCj71TJ6uLRBHwDUIMNwv_s1m00)

See also [another similar graph + explanations](https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-protocols-oauth-code)

Later, a game player id entirely driven by Game Authentication might be interesting to add in order to:
- add another Game Logic Server with shared player id (for a chat module ?)
- add another GLS with different facebook_id (partner not our company)

## Continuous Integration
`cargo test` is run for all branches.
As it's run for pull requests, be careful not to include any sensitive information.

https://blog.jawg.io/docker-multi-stage-build/

## Continuous Delivery
Heroku is used for preproduction, wih automatic deployment of `develop` branch:
https://github.com/emk/heroku-rust-cargo-hello
