FROM rustlang/rust:nightly as builder

RUN rustup component add clippy-preview

# Choose a workdir
WORKDIR /usr/src/app
# Create blank project
RUN USER=root cargo init
# Copy Cargo.toml to get dependencies
COPY Cargo.toml .
# Dummy builds to get the dependencies cached
RUN cargo build
RUN cargo build --release

# Copy sources
# COPY src src

# ENV FACEBOOK_APP_ID=$FACEBOOK_APP_ID
# ENV FACEBOOK_SECRET=$FACEBOOK_SECRET
# ENV GAME_API_SECRET=$GAME_API_SECRET
# ENV RESOURCE_SERVER_GET_ID_URL=$RESOURCE_SERVER_GET_ID_URL
# # Build app (bin will be in /usr/src/app/target/release/auth-server)
# RUN cargo build --release

# FROM debian:stretch
# # install openssl
# RUN apt-get update && apt-get install -y openssl
# # Copy bin from builder to this new image
# COPY --from=builder /usr/src/app/target/release/auth-server /bin/
# # Default command, run app
# CMD auth-server