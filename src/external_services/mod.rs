mod facebook_api;

#[derive(Debug, Serialize, Deserialize)]
pub enum Issuer {
    Facebook,
    ResourceServer,
}

#[derive(Deserialize, Serialize)]
pub struct AuthorizationGrant {
    /// Where does that authorization come from ? Who will we ask to verify this authorization ?
    pub issuer: Issuer,
    /// User id from issuer.
    pub user_id: String,
    /// Access token, should be verified.
    pub access_token: String,
}

pub enum ExternalServiceValidatorError {
    UnknownIssuer,
    Unauthorized,
}

impl From<ExternalServiceValidatorError> for rocket::response::Failure {
    fn from(error: ExternalServiceValidatorError) -> rocket::response::Failure {
        match error {
            ExternalServiceValidatorError::Unauthorized => {
                rocket::response::Failure(rocket::http::Status::Unauthorized)
            }
            ExternalServiceValidatorError::UnknownIssuer => {
                rocket::response::Failure(rocket::http::Status::BadRequest)
            }
        }
    }
}

/// Generic trait to enable dependency injection for testing purposes.
pub trait ExternalServiceValidator {
    fn verify(&self, grant: &AuthorizationGrant) -> Result<(), ExternalServiceValidatorError>;
}

pub struct FacebookValidator<'a> {
    pub facebook_app_id: &'a str,
    pub facebook_secret: &'a str,
}

impl<'a> ExternalServiceValidator for FacebookValidator<'a> {
    fn verify(&self, grant: &AuthorizationGrant) -> Result<(), ExternalServiceValidatorError> {
        match grant.issuer {
            Issuer::Facebook => match facebook_api::verify_token(
                self.facebook_app_id,
                self.facebook_secret,
                &grant.access_token,
            ) {
                Ok(token_verification) => {
                    if !token_verification.data.is_valid
                        || grant.user_id != token_verification.data.user_id
                    {
                        return Err(ExternalServiceValidatorError::Unauthorized);
                    }
                    Ok(())
                }
                Err(_) => Err(ExternalServiceValidatorError::Unauthorized),
            },
            _ => Err(ExternalServiceValidatorError::UnknownIssuer),
        }
    }
}
