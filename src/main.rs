#![feature(plugin)]
#![feature(tool_lints)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
extern crate dotenv;
extern crate jsonwebtoken;
extern crate reqwest;
extern crate serde_json;
#[macro_use]
extern crate dotenv_codegen;

mod external_services;
mod routes_definitions;

use dotenv::dotenv;

fn main() {
    dotenv().ok();
    rocket::ignite()
        .manage(Box::new(external_services::FacebookValidator {
            facebook_app_id: dotenv!("FACEBOOK_APP_ID"),
            facebook_secret: dotenv!("FACEBOOK_SECRET"),
        }))
        .manage(Box::new(
            routes_definitions::resource_server_api::GenericServerAPI {
                url: dotenv!("RESOURCE_SERVER_GET_ID_URL"),
                secret: dotenv!("GAME_API_SECRET"),
            },
        ))
        .mount("/", routes![routes_definitions::authorize])
        .mount("/", routes![routes_definitions::status])
        .launch();
}
