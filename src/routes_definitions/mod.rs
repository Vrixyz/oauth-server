#![allow(clippy::needless_pass_by_value)]
#![allow(clippy::borrowed_box)]

pub mod resource_server_api;

use external_services::{AuthorizationGrant, ExternalServiceValidator, Issuer};
use rocket::State;
use rocket_contrib::Json;

#[get("/status")]
pub fn status() -> Result<String, rocket::response::Failure> {
    Ok(String::from("UP"))
}

#[post(
    "/authorize",
    data = "<authorization_grant>",
    format = "application/json"
)]
pub fn authorize(
    authorization_grant: Json<AuthorizationGrant>,
    validator: State<Box<ExternalServiceValidator + Send + Sync>>,
    resource_api: State<Box<resource_server_api::ResourceServerAPI + Send + Sync>>,
) -> Result<String, rocket::response::Failure> {
    authorize_private(authorization_grant, validator.inner(), resource_api.inner())
}

fn authorize_private(
    authorization_grant: Json<AuthorizationGrant>,
    validator: &Box<ExternalServiceValidator + Send + Sync>,
    resource_api: &Box<resource_server_api::ResourceServerAPI + Send + Sync>,
) -> Result<String, rocket::response::Failure> {
    let grant = authorization_grant.into_inner();
    match validator.verify(&grant) {
        Err(_) => Err(rocket::response::Failure(
            rocket::http::Status::Unauthorized,
        )),
        Ok(_) => {
            println!("Token was correct! getting user id from resource server");
            create_token(
                resource_api.retrieve_user_id(&grant.user_id)?,
                resource_api.get_secret(),
            )
        }
    }
}

#[derive(Debug, Serialize)]
struct Claims {
    iss: Issuer,
    sub: String,
}

/// Method for the final encoding of a client's credentials.
/// The returned String is a json web token encoding a `Claims` structure.
/// `sub` is user id, provided by `iss` (probably a game server)
fn create_token(sub: String, secret: &str) -> Result<String, rocket::response::Failure> {
    let claims = Claims {
        iss: Issuer::ResourceServer,
        sub,
    };
    println!("claims: {:#?}", claims);
    match jsonwebtoken::encode(&jsonwebtoken::Header::default(), &claims, secret.as_ref()) {
        Ok(token) => match serde_json::to_string(&token) {
            Ok(ret) => Ok(ret),
            Err(_) => Err(rocket::response::Failure(rocket::http::Status::NotFound)),
        },
        Err(_) => Err(rocket::response::Failure(rocket::http::Status::BadRequest)),
    }
}

#[cfg(test)]
mod tests {
    use external_services::ExternalServiceValidator;
    use routes_definitions::resource_server_api;
    use *;

    struct FakeExternalServiceValidator;
    impl external_services::ExternalServiceValidator for FakeExternalServiceValidator {
        fn verify(
            &self,
            _: &external_services::AuthorizationGrant,
        ) -> Result<(), external_services::ExternalServiceValidatorError> {
            Ok(())
        }
    }
    struct FakeResourceServerAPI<'a> {
        pub real_id: &'a str,
    }
    impl<'a> resource_server_api::ResourceServerAPI<'a> for FakeResourceServerAPI<'a> {
        fn retrieve_user_id(
            &self,
            _: &str,
        ) -> Result<String, resource_server_api::GameServerError> {
            return Ok(self.real_id.to_string());
        }
        fn get_secret(&self) -> &'a str {
            "fakesecret"
        }
    }

    /// To debug this test, check out https://jwt.io/
    #[test]
    fn logic_and_jwt_creation() -> Result<(), rocket::response::Failure> {
        let authorization_grant = rocket_contrib::Json(external_services::AuthorizationGrant {
            issuer: external_services::Issuer::Facebook,
            user_id: "fakeid".to_string(),
            access_token: "faketoken".to_string(),
        });
        let validator: Box<ExternalServiceValidator + Send + Sync> =
            Box::new(FakeExternalServiceValidator {});
        let resource_api: Box<resource_server_api::ResourceServerAPI + Send + Sync> =
            Box::new(FakeResourceServerAPI { real_id: "real_id" });
        let jsonwebtoken =
            routes_definitions::authorize_private(authorization_grant, &validator, &resource_api)?;
        let expected_json_webtoken = "\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJSZXNvdXJjZVNlcnZlciIsInN1YiI6InJlYWxfaWQifQ.hm9SjeyxYP8DTQJife_zSaDb9mG3jHAFL9npHknSiUo\"";
        println!("test computed jwt: {}", jsonwebtoken);
        println!("then expected jwt: {}", expected_json_webtoken);

        assert!(String::from(jsonwebtoken) == String::from(expected_json_webtoken));
        Ok(())
    }
}
