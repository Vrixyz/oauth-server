use reqwest::{Body, Client};

pub enum GameServerError {
    BadRequest,
    ReqwestInitialization,
    BadResponse,
}

#[derive(Debug, Serialize)]
struct Parameters {
    facebook_id: String,
}

#[derive(Debug, Deserialize)]
pub struct ResourceServerId {
    pub id: i32,
    pub name: String,
    pub external_facebook_id: String,
}
impl From<GameServerError> for rocket::response::Failure {
    fn from(resource: GameServerError) -> rocket::response::Failure {
        match resource {
            GameServerError::BadRequest => {
                rocket::response::Failure(rocket::http::Status::BadRequest)
            }
            GameServerError::ReqwestInitialization => {
                rocket::response::Failure(rocket::http::Status::InternalServerError)
            }
            GameServerError::BadResponse => {
                rocket::response::Failure(rocket::http::Status::InternalServerError)
            }
        }
    }
}

pub trait ResourceServerAPI<'a> {
    fn retrieve_user_id(&self, facebook_id: &str) -> Result<String, GameServerError>;
    fn get_secret(&self) -> &'a str;
}

pub struct GenericServerAPI<'a> {
    pub url: &'a str,
    pub secret: &'a str,
}

impl<'a> ResourceServerAPI<'a> for GenericServerAPI<'a> {
    fn retrieve_user_id(&self, facebook_id: &str) -> Result<String, GameServerError> {
        match serde_json::to_string(&Parameters {
            facebook_id: facebook_id.to_string(),
        }) {
            Ok(parameters) => match Client::new()
                .post(self.url)
                .body(Body::from(parameters))
                .send()
            {
                Ok(mut result) => match result.json::<ResourceServerId>() {
                    Ok(resource_server_id) => Ok(resource_server_id.id.to_string()),
                    Err(_) => Err(GameServerError::BadResponse),
                },
                Err(_) => Err(GameServerError::ReqwestInitialization),
            },
            Err(_) => Err(GameServerError::BadRequest),
        }
    }
    fn get_secret(&self) -> &'a str {
        self.secret
    }
}
