#!/bin/bash

ENV_DIR=${3:-}

if [ ! -f '.env~' ]; then mv .env '.env~'; fi;

echo "FACEBOOK_APP_ID=$(cat $ENV_DIR/FACEBOOK_APP_ID)
FACEBOOK_SECRET=$(cat $ENV_DIR/FACEBOOK_SECRET)
GAME_API_SECRET=$(cat $ENV_DIR/GAME_API_SECRET)
RESOURCE_SERVER_GET_ID_URL=$(cat $ENV_DIR/RESOURCE_SERVER_GET_ID_URL)" \
 > .env

cat .env